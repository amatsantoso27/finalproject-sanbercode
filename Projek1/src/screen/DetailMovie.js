import React, {Component} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class DetailMovie extends Component {
  render() {
    const {name} = this.props.route.params;
    const {year} = this.props.route.params;
    const {image} = this.props.route.params;
    const {overview} = this.props.route.params;
    const {rate} = this.props.route.params;
    const IMAGE_DOMAIN_URL = 'https://image.tmdb.org/t/p/w500';
    return (
      <View style={styles.container}>
        <Text style={styles.header}> Detail Movie </Text>
        <Image
          source={{uri: IMAGE_DOMAIN_URL + image}}
          style={{height: 180, width: 320, alignSelf: 'center', borderRadius: 5}}
        />
        <View style={styles.body}>
          <View
            style={{
              flexDirection: 'row',
              paddingBottom: 15,
              flexWrap: 'wrap',
              justifyContent: 'center'
            }}>
            <Text style={styles.judul}>{name}</Text>
            <Text style={styles.isiBody}>( {year} )</Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon
                name="star"
                size={10}
                style={{alignContent: 'center', marginRight: 2}}
              />
              <Text>{rate}</Text>
            </View>
          </View>
          <Text style={{textAlign: 'center', fontSize: 16, fontWeight: 'bold'}}>
            Deskripsi:
          </Text>
          <Text style={{textAlign: 'center', fontSize: 16}}>{overview} </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  header: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 15,
    textShadowRadius: 5,
  },
  body: {
    margin: 25,
    flex: 1
  },
  judul: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingRight: 4,
  },
  isiBody: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingRight: 4,
  },
});
