import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

// API DOMAIN
const API_URL = 'https://api.themoviedb.org/3';
// API NAME Get Trending By Week
const API_NAME = '/movie/popular';
// API KEY For Securty
const API_KEY = 'eaec1e9c75863d88083224da5c43ffa7';
const IMAGE_DOMAIN_URL = 'https://image.tmdb.org/t/p/w500/';
//EXAMPLE to get Movies List Hot Trends
//https://api.themoviedb.org/3/trending/all/week?api_key=your_api_key

class MyMovieItem extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {item} = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          margin: 12,
          padding: 10,
          borderTopEndRadius: 8,
          borderTopStartRadius: 8,
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 10,
          shadowColor: "#ecf0f1",
          shadowOffset: {
            width: 0,
            height: 8,
          },
          shadowOpacity: 0.46,
          shadowRadius: 11.14,
          
          elevation: 3,
        }}>
        <Image
          style={{height: 225, width: 140}}
          source={{uri: IMAGE_DOMAIN_URL + item.poster_path}}
        />
        <View style={{marginLeft: 10, flex: 1}}>
          <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.title}</Text>
          <Text style={{fontSize: 15}}>Rating : {item.vote_average} </Text>
          <Text style={{fontSize: 15}}>
            Release : {item.release_date.slice(0, 4)}
          </Text>
        </View>
      </View>
    );
  }
}
export default class Movie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
    };
  }
  componentDidMount() {
    this.fetchMovies();
  }
  fetchMovies() {
    fetch(API_URL + API_NAME + '?api_key=' + API_KEY)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({movies: responseJson});
      })
      .catch((error) => {
        console.log('Data fetching failed');
      });
  }
  render() {
    const {navigation} = this.props;
    const {movies} = this.state;
    const {item} = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.header}>List Popular Movies</Text>
        {movies && movies.results && movies.results.length > 0 ? (
          // <TouchableOpacity onPress={() => navigation.navigate('DetailMovie', {name: item.title, year: item.release_date.slice(0, 4)})}>
          <FlatList
            data={movies.results}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('DetailMovie', {
                    name: item.title,
                    year: item.release_date.slice(0, 4),
                    image: item.backdrop_path,
                    overview: item.overview,
                    rate: item.vote_average,
                  })
                }>
                <MyMovieItem item={item} />
              </TouchableOpacity>
            )}
            keyExtractor={(item, key) => key.toString()}
            //onRefresh={this.onRefresh}
            //refreshing={this.state.refreshing}
          ></FlatList>
        ) : // </TouchableOpacity>
        null}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  header: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 15,
    textShadowRadius: 5,
  },
});
